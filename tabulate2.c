#include <stdio.h>
#include <math.h>

#define XMIN 1.0
#define XMAX 10.0
#define N 10

int main(void){
	int i; 
	double x, y, z;
	for(i=0;i<N;i++){
		x=XMIN+(XMAX-XMIN)/(N-1.0)*i;
		y=sin(x);
		z=cos(x);
		printf("%f %f %f\n",x,y,z);
	}
	return 0;
}

