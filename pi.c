#include<stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used .", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */

#define N 10000000

double f(double x);

double pi(long n);

int main(void) {
    /* Declarations */



    /* Code */
    START;               /* Timing measurement starts here */
	
	printf("The value of pi calculated with N = %d is: %1.8f",N,pi(N));
	

    STOP;                /* Timing measurement stops here */
    PRINTTIME;           /* Print timing results */
    return 0;
}




double f(double x){
	return sqrt(1-pow(x,2));
}

double pi(long n){
	double a = -1;
	double b = 1;
	double h = (b-a)/N;
	double s = 0.5*f(a) + 0.5*f(b);
	int i;
	double pi, x;;
	for(i=0;i<N;i++){
		x = a + i*h;
		s = s+ f(x);
	}
	pi = s*h*2;
	return pi;
}

