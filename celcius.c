#include <stdio.h>

int main(void){
	int C;
	double F;
	for(C=-30;C<32;C=C+2){
		F = (C*9.0)/5.0+32;
		printf("%3d = %3.1f\n",C,F);
	}
	return 0;
}

