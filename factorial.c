#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void);

double upper_bound(long n);

long factorial(long);

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
	
    printf("Factorial of 5 is %ld",factorial(500));
    
	return 0;
}

long maxlong(void){
	return LONG_MAX;
}

double upper_bound(long n){
	if(n>=6){
		double U=(n/2.0);
		return pow(U,n);
	}
	else {
		return 719;
	}
}

long factorial(long n){
	long i;
	long j=1;
	if(n>=0){
		if(upper_bound(n)<LONG_MAX){ 
			for (i=n;i>1;i--){
				j=j*i;
			}
		}
		else {
			return -1;
		}
	}
	else {
		return -2;
	}
	return j;
}

