#include <stdio.h>
#include <string.h>

#define MAXLINE 1000 /* maximum length of string */


long string_length(char s[]);

void reverse(char source[], char target[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

long string_length(char s[]){
	int i, j;
	j=0;
	for (i=0;s[i]!='\0';i++){
		j=j+1;
	}
	return j;
}

void reverse(char source[], char target[]){
	int i;
	int len = string_length(source)-1;
	for(i=0;i<=len;i++){
		target[i] = source[len-i];
	}
	target[len+1] = '\0';
}

