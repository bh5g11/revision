#include <stdio.h>
#include <math.h>

#define XMIN 1.0
#define XMAX 10.0
#define N 10

int main(void){
	int i; 
	double x, y;
	for(i=0;i<N;i++){
		x=XMIN+(XMAX-XMIN)/(N-1.0)*i;
		y=sin(x);
		printf("%f %f\n",x,y);
	}
	return 0;
}

