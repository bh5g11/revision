#include <stdio.h>

int main(void){
	double s=1000;
	double debt=s;
	double rate=0.03;
	double interest;
	int i;
	for(i=1;i<25;i++){
		interest = debt*rate;
		debt = debt +interest;
		printf("month %2d: debt=%7.2f, total_interest=%7.2f, frac= %7.2f%%\n",i,debt,debt-1000,(debt-1000)/10);
	}
	return 0;
}

