#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* mix(char *s1, char *s2){
	
	int i, j, k;
	int len = strlen(s1) + strlen(s2)+1;
	
	char *sn;
	sn = (char *)malloc(sizeof(char)*len);
	
	if(sn == NULL){
		printf("Memory allocation failed\n");
		return NULL;
	}
	
	for(i=0,j=1, k=0;i<len;i=i+2,j=j+2,k++){
		sn[i]=s1[k];
		sn[j]=s2[k];
	}
	sn[len]='\0';
	return sn;
}

int main(void) {
    char s1[] = "Hello World";
    char s2[] = "1234567890!";

    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("sn  = %s\n", mix(s1, s2));
	return 0;
}
	

