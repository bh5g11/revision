/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>
#include <string.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

void lstrip(char s[]){
	int i=0;
	int j;
	int k;
	while(s[i]==' '){
		i++;
	}
	k = strlen(s)-i;
	for(j=0;j<k;j++){
		s[j]=s[i+j];
	}
	s[k] = '\0';
}

int main(void) {
  char test1[] = "   Hello World";

  printf("Original string reads  : |%s|\n", test1);
  lstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}

